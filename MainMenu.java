/*
 * MovieGenerator
 * This program is used to to store movies easily
 * and for me to repeat things I have "learned"
 * in the previous Java programming course.
 * 
 * @author Viktor Eklöv
 * @version 0.1
 */

package moviegenerator;

/**
 * Interface MainMenu
 * Has abstract methods that of a simple MainMenu
 * 
 * @author Viktor Eklöv
 * @version 0.1
 */
public interface MainMenu {
    /**
     * Method printMenu
     * Supposed to contain code for printing a menu
     * to the user
     * and the methods that are part of the Menu
     * in MovieMenu
     */
    void printMenu();
    
    /**
     * Method showMenuChoice
     * Supposed to contain code for calling methods corresponding
     * to the user's input
     * 
     * @param choice The integer input of the user
     */
    void showMenuChoice(int choice);
    
    /**
     * Method quitChoice
     * used to quick the program supposed to be empty
     */
    void quitChoice();
    
    /**
     * Method secondaryMenu
     * supposed to contain code to show the secondary menu
     */
    void secondaryMenu();
    
    /**
     * Method printMovieList
     * supposed to contain code to print all the movies to the user
     */
    void printMovieList();
    
    /**
     * Method addMovieChoice
     * supposed to contain code to add a movie to the list
     */
    void addMovieChoice();
    
    /**
     * Method removeMovieChoice
     * supposed to contain code to remove a movie from the list
     */
    void removeMovieChoice();
    
    /**
     * Method userIntegerInput
     * supposed to contain code to get an Integer from the user
     * 
     * @return Integer from user
     */
    int userIntegerInput();
}
