/**
 * MovieGenerator
 * This program is used to to store movies easily
 * and for me to repeat things I have "learned"
 * in the previous Java programming course.
 *
 * @author Viktor Eklöv
 * @version 0.1
 */

package moviegenerator;

/**
 * Class MovieGenerator
 * This class is the starting point of the program
 * It does not do anything besides start it up
 * 
 * @author Viktor Eklöv
 * @version 0.1
 */
public class MovieGenerator {
    /**
     * Main method
     * Starts up the program by creating a MovieMenu
     * and calling the printMenu() method
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MovieMenu menu = new MovieMenu();
        menu.printMenu();
    }
    
}
