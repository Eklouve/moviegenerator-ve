/**
 * MovieGenerator
 * This program is used to to store movies easily
 * and for me to repeat things I have "learned"
 * in the previous Java programming course.
 *
 * @author Viktor Eklöv
 * @version 0.1
 */

package moviegenerator;
import java.util.Scanner;
/**
 * Class MovieMenu
 * Handles the user input and output
 * and calls corresponding methods for user's input
* to act as a menu for the program
 * 
 * @author Viktor Eklöv
 * @version 0.1
 */
public class MovieMenu implements MainMenu{
    /**
     * The MovieList containing a list of the movies
     */
    private final MovieList myMovieList = new MovieList();
    /**
     * The Scanner that scans user input
     */
    private final Scanner myScanner = new Scanner(System.in);
    
    /**
     * Method printMenu
     * Prints out the menu for user to see
     * and takes in a input from user that is sent to
     * showMenuChoice() method.
     */
    public void printMenu()
    {
        System.out.println("Welcome to MovieGenerator!");
        System.out.println("What do you want to do?");
        System.out.println("----------------------------------");
        System.out.println("1. Watch List of Movies");
        System.out.println("2. Add a Movie");
        System.out.println("3. Remove a Movie");
        System.out.println("----------------------------------");
        System.out.println("9. Exit");
        int choice = 0;
        while (true)
        {
            choice = userIntegerInput();
            if (choice == 1 || choice == 2 || choice == 3 || choice == 9)
            {
               break;
            }
            System.out.println("Invalid choice! Try again.");
        }
        showMenuChoice(choice);
    }
    
    /**
     * Method printMovieList
     * Prints out the info of all the movies
     * contained in the list
     */
    public void printMovieList()
    {
        for(int i = 0; i < myMovieList.getSize(); i++)
        {
            System.out.println("----------------------------------");
            System.out.println("Title: " + myMovieList.getMovieInfo(i)[0]);
            System.out.println("Time: " + myMovieList.getMovieInfo(i)[1]);
            System.out.println("Genre: " + myMovieList.getMovieInfo(i)[2]);
            System.out.println("Director: " + myMovieList.getMovieInfo(i)[3]);
            System.out.println("Age: " + myMovieList.getMovieInfo(i)[4]);
        }
        secondaryMenu();
    }
    
    /**
     * Method showMenuChoice
     * Calls the corresponding method for the choice of the user
     * using a switch
     * 
     * @param choice The choice of the user as integer
     */
    public void showMenuChoice(int choice)
    {
        switch (choice)
        {
            case 1: printMovieList(); break;
            case 2: addMovieChoice(); break;
            case 3: removeMovieChoice(); break;
            case 9: quitChoice(); break;
            default: printMovieList();
        }
    }
    
    /**
     * Method addMovieChoice
     * Adds a movie to the list by scanning in
     * the information about the movie needed and 
     * then calling the Movie2List method to add it
     * to the list
     */
    public void addMovieChoice()
    {
        String title;
        String genre;
        String director;
        int time;
        int age;
        System.out.println("Add a movie:");
        System.out.println("Title:");
        title = myScanner.nextLine();
        System.out.println("Time:");
        time = userIntegerInput();
        System.out.println("Genre:");
        genre = myScanner.nextLine();
        System.out.println("Director:");
        director = myScanner.nextLine();
        System.out.println("Age:");
        age = userIntegerInput();
        Movie2List(title, time, genre, director, age);
        secondaryMenu();
    }
    
    /**
     * Method Movie2List
     * Adds a movie to the list by calling
     * MovieList.addMovie() method
     * 
     * @param inTitle The title of the movie
     * @param inTime The length of the movie
     * @param inGenre The genre of the movie
     * @param inDirector The name of the director of the movie
     * @param inAge The age of the movie, how long it has been released
     */
    private void Movie2List(String inTitle, int inTime, String inGenre, String inDirector, int inAge)
    {
        if (myMovieList.addMovie(inTitle, inTime, inGenre, inDirector, inAge))
        {
            System.out.println("Movie Added! " + inTitle);
        }
        else
        {
            System.out.println("An error has occurred! Movie could not be added!");
        }
        System.out.println("Number of Movies: " + myMovieList.getSize());
    }
    
    /**
     * Method removeMovieChoice
     * Removes a movie from the list by
     * printing the movie titles and getting
     * a integer input from user.
     */
    public void removeMovieChoice()
    {
        if (myMovieList.getSize() == 0)
        {
            System.out.println("There are no movies");
            printMenu();
        }
        for(int i = 0; i < myMovieList.getSize(); i++)
        {
            System.out.println("-----------------Movie Number: " + (i + 1) + "-----------------");
            System.out.println("Title: " + myMovieList.getMovieInfo(i)[0]);
        }
        System.out.println("Enter Number to Erase:");
        while (true)
        {
            int input = userIntegerInput();
            if (!myMovieList.removeMovie(input - 1))
            {
                System.out.println("Invalid choice! Try again");
            }
            else
            {
                break;
            }
        }
        printMenu();
    }
    
    /**
     * Method quitChoice
     * Empty method, called when user
     * wants to quit the program
     * Because it is empty it will make us go
     * back to the main method and let the program
     * finish and automatically end/quit
     */
    public void quitChoice()
    {
        
    }
    
    /**
     * Method userIntegerInput
     * A method used to get a integer input
     * from the user safely, forcing the user
     * to try again until a integer has been inputed
     * 
     * @return 
     */
    public int userIntegerInput()
    {
        int input = 0;
        while (true)
        {
            try
            {
                input = Integer.parseInt(myScanner.nextLine().trim());
                break;
            }
            catch (NumberFormatException e)
            {
                System.out.println("Invalid input! Try again!");
            }
        }
        return input;
    }
    
    /**
     * Method secondaryMenu
     * Shows the secondary method
     * and calls the corresponding methods to the choice of the user
     */
    public void secondaryMenu()
    {
        System.out.println("0 = Back to HeadMenu    9 = Exit Program");
        int choice = 0;
        while (true)
        {
            choice = userIntegerInput();
            if (choice == 0 || choice == 9)
            {
                break;
            }
            System.out.println("Invalid choice! Try again.");
        }
        switch (choice)
        {
            case 0: printMenu(); break;
            case 9: quitChoice(); break;
            default: printMenu();
        }   
    }
}
