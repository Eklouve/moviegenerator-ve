/**
 * MovieGenerator
 * This program is used to to store movies easily
 * and for me to repeat things I have "learned"
 * in the previous Java programming course.
 *
 * @author Viktor Eklöv
 * @version 0.1
 */

package moviegenerator;
import java.util.ArrayList;
/**
 * Class MovieList
 * This class is used to contain all the movies in a list and easily manage them
 * for me using its methods.
 * 
 * @author Viktor Eklöv
 * @version 0.1
 */
public class MovieList {
    /**
     * ArrayList containing the movies
     */
    private final ArrayList<Movie> myMovies;
    
    /**
     * Constructor for Class MovieList
     * Sets up the list containing the movies
     * so we don't get null pointer exception
     */
    MovieList()
    {
        myMovies = new ArrayList<>();
    }
    
    /**
     * Method addMovie
     * Adds a new movie to the list
     * 
     * @param inTitle The title of the movie
     * @param inTime The length of the movie
     * @param inGenre The genre of the movie
     * @param inDirector The name of the director of the movie
     * @param inAge The age of the movie, how long ago it was released
     * @return true if it was added, false if it could not be added
     */
    public boolean addMovie(String inTitle, int inTime, String inGenre, String inDirector, int inAge)
    {
        return myMovies.add(new Movie(inTitle, inTime, inGenre, inDirector, inAge));
    }
    
    /**
     * Method getMovieInfo
     * Gets all the info from the selected movie and returns it
     * 
     * @param choice Index of the movie to get info from
     * @return The info of the movie as a string array
     */
    public String[] getMovieInfo(int choice)
    {
        return myMovies.get(choice).to_String();
    }
    
    /**
     * Method removeMovie
     * Removes the selected movie
     * 
     * @param choice Index of the movie to remove
     * @return true if it could be removed, false if it could not be removed
     */
    public boolean removeMovie(int choice)
    {
        try
        {
            if (!myMovies.remove(choice).equals(true))
            {
                return true;
            } 
            else 
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }
        
    }
    
    /**
     * Method getSize
     * Gets the size of the list and returns it
     * 
     * @return The size of the list as an integer
     */
    public int getSize()
    {
        return myMovies.size();
    }
}
