Project: MovieGenerator

Project Info: This is a program to easily store and manage movies, and to repeat things that I have "Learned"
in the previous Java programming course I have had.

MovieGenerator.java - this is the main class
MovieMenu.java - the menu for the user, handles all input and output, implements MainMenu.java (interface)
MovieList.java - class for managing movies
Movie.java - the structure of a movie and get methods for the info
MainMenu.java - Interface, abstract methods used to output to the user is in here.

Some methods were removed, took away methods that did nothing or was not needed for the solution.

Author: Viktor Ekl�v
Version: 0.1

HOW TO START THE PROGRAM:
1. Open terminal/CMD, on windows computers you can do this by pressing WindowsKey + R at the same time
and then writing "cmd" and press enter.
2. Navigate to the folder named "dist" ( Short for distribution ) by writing "cd [insert file path here]"
if you're on a MAC, then google is your friend.
3. Write "java -jar "MovieGenerator.jar"" and press enter
4. Enjoy the program