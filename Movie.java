/**
 * MovieGenerator
 * This program is used to to store movies easily
 * and for me to repeat things I have "learned"
 * in the previous Java programming course.
 *
 * @author Viktor Eklöv
 * @version 0.1
 */

package moviegenerator;

/**
 * Class Movie
 * User to get some structure for every movie which
 * makes it easy to create and delete movies and use them
 * in other classes
 * 
 * @author Viktor Eklöv
 * @version 0.1
 */
public class Movie {
    int time;
    int age;
    String title;
    String genre;
    String director;
    
    /**
     * Constructor for Class Movie
     * Sets up the variables upon creation of a instance of the class.
     * 
     * @param inTitle The title of the movie
     * @param inTime The length of the movie
     * @param inGenre The genre of the movie
     * @param inDirector The name of the director of the movie
     * @param inAge The age of the movie, how long ago it was released
     */
    Movie(String inTitle, int inTime, String inGenre, String inDirector, int inAge){
        time = inTime;
        age = inAge;
        title = inTitle;
        genre = inGenre;
        director = inDirector;
    }
    
    /**
     * Method getTitle
     * Returns the title of the movie as a string
     * 
     * @return The title of the movie
     */
    public String getTitle(){return title;}
    
    /**
     * Method getTime
     * Returns the length of the movie as a integer
     * 
     * @return The length of the movie
     */
    public int getTime(){return time;}
    
    /**
     * Method getAge
     * Returns the age of the movie as a integer
     * 
     * @return The age of the movie
     */
    public int getAge(){return age;}
    
    /**
     * Method getGenre
     * Returns the genre of the movie as a string
     * 
     * @return The genre of the movie
     */
    public String getGenre(){return genre;}
    
    /**
     * Method getDirector
     * Returns the name of the director of the movie as a string
     * 
     * @return The name of the director
     */
    public String getDirector(){return director;}
    
    /**
     * Method to_String
     * Returns all the variables of the movie as a string array
     * 
     * @return String array with all the variables of the movie
     */
    public String[] to_String() {
        String[] result = new String[5];
        result[0] = title;
        result[1] = Integer.toString(time);
        result[2] = genre;
        result[3] = director;
        result[4] = Integer.toString(age);
        return result;
    }
}
